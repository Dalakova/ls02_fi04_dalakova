package kreis;

import java.awt.Point;

public class Rechteck {
	private double high;
	private double wide;
	private Point startpunkt;
    
	public Rechteck(double high, double wide, Point startpunkt)
	{
	  setStartpunkt(startpunkt);
	  setHigh(high);
	  setWide(wide);
	}
	
	public void setHigh(double high) {
		if(high > 0)
			this.high = high;
		else
			this.high = 0;
	}
	
	public double getHigh() {
		return this.high;
	}
	
	public void setWide(double wide) {
		if(wide > 0)
			this.wide = wide;
		else
			this.wide = 0;
	}
	
	public double getWide() {
		return this.wide;
	}
	
	public double getDurchmesser() {
		return Math.sqrt(Math.pow(this.high, 2) + Math.pow(this.wide, 2));
	}
	
	public double getFlaeche() {
		return this.high * this.wide;
	}
	
	public double getUmfang() {
		return 2 * this.high + 2 * this.wide;
	}

	public void setStartpunkt(Point startpunkt) {
		this.startpunkt = startpunkt; 
	}
	
	public Point getStartpunkt() {
		return this.startpunkt;
	}
}
