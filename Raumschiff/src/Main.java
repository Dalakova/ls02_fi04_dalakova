import java.util.ArrayList;

/**
 * @author Diana Dalakova
 * @version 1.0
 * @since 08.03.2021
 */
public class Main {
	
	public static void main(String[] args) {
		
		//create broadcast channel
		ArrayList<String> broadcast = new ArrayList<String>();
		
		//create cargo
		Ladung saft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung schrott = new Ladung("Borg-Schrott", 5);
		Ladung rMaterie = new Ladung("Rote Materie", 2);
		Ladung sonde = new Ladung("Forschungssonde", 35);
		Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung waffe = new Ladung("Plasma-Waffe", 50);
		Ladung torpedo = new Ladung("Photonentorpedo", 3);
		
		//create spaceships
		Raumschiff klingonSchiff = new Raumschiff("IKS Hegh'ta", "", 100, 100, 100, 100, 1, 2, broadcast);
		Raumschiff romulanSchiff = new Raumschiff("IRW Khazara", "", 100, 100, 100, 100, 2, 2, broadcast);
		Raumschiff vulkanierSchiff = new Raumschiff("Ni'Var", "", 80, 80, 100, 50, 0, 5, broadcast);
		
		//set the cargo to the ships
		klingonSchiff.addLadung(saft);
		klingonSchiff.addLadung(schwert);
		
		romulanSchiff.addLadung(schrott);
		romulanSchiff.addLadung(rMaterie);
		romulanSchiff.addLadung(waffe);
		
		vulkanierSchiff.addLadung(sonde);
		vulkanierSchiff.addLadung(torpedo);
		
		//first round of actions
		klingonSchiff.photonentorpedoAbschießen(romulanSchiff);
		romulanSchiff.phaserkanoneAbschießen(klingonSchiff);
		vulkanierSchiff.nachrichtSenden("Gewalt ist nicht logisch");
	
		//clingon checks the state of their ship and cargo
		klingonSchiff.raumschiffZustandZeigen();
		klingonSchiff.ladungsverzeichnisZeigen();
		
		//second round of actions
		klingonSchiff.photonentorpedoAbschießen(romulanSchiff);
		klingonSchiff.photonentorpedoAbschießen(romulanSchiff);
		
		//all checks the state of the ship and cargo
		klingonSchiff.raumschiffZustandZeigen();
		klingonSchiff.ladungsverzeichnisZeigen();
		romulanSchiff.raumschiffZustandZeigen();
		romulanSchiff.ladungsverzeichnisZeigen();
		vulkanierSchiff.raumschiffZustandZeigen();
		vulkanierSchiff.ladungsverzeichnisZeigen();
	}
}
