/**
 * @author Diana Dalakova
 * @version 1.0
 * @since 08.03.2021
 */
public class Ladung {
	//parameters of the class Ladung
	private String type;
	private int anzahl;
	private String raumschiffname;
	
	/**
	 * Empty constructor for class Ladung
	 */
	public Ladung () {}
	
	/**
	 * full-parameter constructor of the class Ladung
	 * @param type specify a type of the cargo
	 * @param anzahl specify an amount of the cargo, should be > 0
	 */
	public Ladung (String type, int anzahl) {
		setType(type);
		setAnzahl(anzahl);
		setRaumschiffname("");
	}
	
	//getters and setters of the class Ladung
	public void setType (String type) {
		this.type = type;
	}
	public String getType () { return this.type; }
	
	public void setAnzahl (int anzahl) {
		if (anzahl > 0)
			this.anzahl = anzahl;
		else
			this.anzahl = 0;
	}
	public int getAnzahl () { return this.anzahl; }
	
	public void setRaumschiffname(String raumschiffname) {
		this.raumschiffname = raumschiffname;
	}
	public String getRaumschiffname() { return this.raumschiffname; }
	
}
