import java.util.ArrayList;
import java.util.List;

/**
 * @author Diana Dalakova
 * @version 1.0
 * @since 08.03.2021
 */
public class Raumschiff {
	//parameters of the class Raumschiff
	private String schiffname;
	private String kapitaenname;
	private int energieversorgung_zustand;
	private int schutzschilde_zustand;
	private int lebenserhaltungssysteme_zustand;
	private int huelle_zustand;
	private int photonentorpedos_anzahl;
	private int reparatur_androiden_anzahl;
	public ArrayList<String> broadcast_kommunikator = new ArrayList<String>();
	private List<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	/**
	 * empty constructor of the class Raumschiff
	 */
	public Raumschiff () {}
	
	/**
	 * full-parameter constructor of the class Raumschiff
	 * @param schiffname a name of the spaceship
	 * @param kapitaenname a name of the captain
	 * @param energieversorgung_zustand in % from 0 to 100
	 * @param schutzschilde_zustand in % from 0 to 100
	 * @param lebenserhaltungssysteme_zustand in % from 0 to 100
	 * @param huelle_zustand in % from 0 to 100
	 * @param photonentorpedos_anzahl specify an amount of photon torpedoes, should be >= 0
	 * @param reparatur_androiden_anzahl specify an amount of repair-androids, should be >= 0
	 * @param broadcast_kommunikator common channel for messages
	 */
	public Raumschiff (String schiffname, String kapitaenname, 
					   int energieversorgung_zustand, int schutzschilde_zustand, int lebenserhaltungssysteme_zustand, 
					   int huelle_zustand, int photonentorpedos_anzahl, int reparatur_androiden_anzahl,
					   ArrayList<String> broadcast_kommunikator)
		{
			setSchiffname(schiffname);
			setKapitaenname(kapitaenname);
			setEnergieversorgungZustand(energieversorgung_zustand);
			setSchutzschildeZustand(schutzschilde_zustand);
			setLebenserhaltungssystemeZustand(lebenserhaltungssysteme_zustand);
			setHuelleZustand(huelle_zustand);
			setPhotonentorpedosAnzahl(photonentorpedos_anzahl);
			setReparaturAndroidenAnzahl(reparatur_androiden_anzahl);
			setBroadcastKommunikator(broadcast_kommunikator);
		}
	
	//getters and setters for all parameters
	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}
	public String getSchiffname() { return this.schiffname; }
	
	public void setKapitaenname(String kapitaenname) {
		this.kapitaenname = kapitaenname;
	}
	public String getKapitaenname() { return this.kapitaenname; }
	
	public void setEnergieversorgungZustand(int energieversorgung_zustand) {
		if (energieversorgung_zustand > 0)
			this.energieversorgung_zustand = energieversorgung_zustand;
		else
			this.energieversorgung_zustand = 0;
	}
	public int getEnergieversorgungZustand() { return this.energieversorgung_zustand; }
	
	public void setSchutzschildeZustand(int schutzschilde_zustand) {
		if (schutzschilde_zustand > 0)
			this.schutzschilde_zustand = schutzschilde_zustand;
		else
			this.schutzschilde_zustand = 0;
	}
	public int getSchutzschildeZustand() { return this.schutzschilde_zustand; }
	
	public void setLebenserhaltungssystemeZustand(int lebenserhaltungssysteme_zustand) {
		if (lebenserhaltungssysteme_zustand > 0)
			this.lebenserhaltungssysteme_zustand = lebenserhaltungssysteme_zustand;
		else
			this.lebenserhaltungssysteme_zustand = 0;
	}
	public int getLebenserhaltungssystemeZustand() { return this.lebenserhaltungssysteme_zustand; }
	
	public void setHuelleZustand(int huelle_zustand) {
		if (huelle_zustand > 0)
			this.huelle_zustand = huelle_zustand;
		else
			this.huelle_zustand = 0;
	}
	public int getHuelleZustand() { return this.huelle_zustand; }
	
	public void setPhotonentorpedosAnzahl(int photonentorpedos_anzahl) {
		if (photonentorpedos_anzahl > 0)
			this.photonentorpedos_anzahl = photonentorpedos_anzahl;
		else
			this.photonentorpedos_anzahl = 0;
	}
	public int getPhotonentorpedosAnzahl() { return this.photonentorpedos_anzahl; }
	
	public void setReparaturAndroidenAnzahl(int reparatur_androiden_anzahl) {
		if (reparatur_androiden_anzahl > 0)
			this.reparatur_androiden_anzahl = reparatur_androiden_anzahl;
		else
			this.reparatur_androiden_anzahl = 0;
	}
	public int getReparaturAndroidenAnzahl() { return this.reparatur_androiden_anzahl; }
	
	public void setBroadcastKommunikator(ArrayList<String> broadcast_kommunikator) {
		this.broadcast_kommunikator = broadcast_kommunikator;
	}
	public ArrayList<String> getBroadcastKommunikator() { return this.broadcast_kommunikator; }
	
	/**
	 * method to add cargo on the board of the spaceship
	 * @param neue_ladung an instance of the class Ladung
	 */
	public void addLadung (Ladung neue_ladung) {
		neue_ladung.setRaumschiffname(getSchiffname());
		this.ladungsverzeichnis.add(neue_ladung);
	}
	
	/**
	 * method to show in a console the state of all parameters of the spaceship
	 */
	public void raumschiffZustandZeigen() {
		System.out.println("Schiffname: " + getSchiffname());
		System.out.println("Kapitänname: " + getKapitaenname());
		System.out.println("Energieversorgung: " + getEnergieversorgungZustand());
		System.out.println("Schutzschilde: " + getSchutzschildeZustand());
		System.out.println("Lebenserhaltungssysteme: " + getLebenserhaltungssystemeZustand());
		System.out.println("Hülle: " + getHuelleZustand());
		System.out.println("Photonentorpedos Anzahl: " + getPhotonentorpedosAnzahl());
		System.out.println("Reparaturandroiden Anzahl: " + getReparaturAndroidenAnzahl());	
	}
	
	/**
	 * method to show in a console the cargo (type and amount) of the spaceship
	 */
	public void ladungsverzeichnisZeigen() {
		System.out.println("Ladungsverzeichnis: ");
		for (Ladung l:this.ladungsverzeichnis) {
			System.out.println(l.getType() + ": " + l.getAnzahl());
		}
	}
	
	/**
	 * method to launch the photon torpedo
	 * @param ziel an instance of the class Raumschiff
	 */
	public void photonentorpedoAbschießen(Raumschiff ziel) {
		int amount = getPhotonentorpedosAnzahl();
		if (amount > 0) {
			nachrichtSenden("Photonentorpedo abgeschossen");
			setPhotonentorpedosAnzahl(amount - 1);
			treffer(ziel);
		}
		else {
			nachrichtSenden("-=*Click*=-");
		}
		
	}
	
	/**
	 * method to launch the phaser canon
	 * @param ziel an instance of the class Raumschiff
	 */
	public void phaserkanoneAbschießen(Raumschiff ziel) {
		int amount = getEnergieversorgungZustand();
		if (amount >= 50) {
			nachrichtSenden("Phaserkanone abgeschossen");
			setEnergieversorgungZustand(amount - 50);
			treffer(ziel);
		}
		else
			nachrichtSenden("-=*Click*=-");
	}
	
	/**
	 * method to note the damage that the target get from a hit
	 * @param ziel an instance of the class Raumschiff
	 */
	public void treffer(Raumschiff ziel) {
		System.out.println(ziel.getSchiffname() + " wurde getroffen!");
		int schield = ziel.getSchutzschildeZustand();
		//if the shield is already destroyed
		if (schield <= 0) {
			int huelle = ziel.getHuelleZustand();
			int energieversorgung = ziel.getEnergieversorgungZustand();
			//check if the shell is already destroyed. If yes, set the state of life system to 0 and send a message about it to all
			if (huelle <= 0) {
				ziel.setLebenserhaltungssystemeZustand(0);
				ziel.nachrichtSenden("Die Lebenserhaltungssysteme sind vernichtet worden.");
			}
			//if the shell is not yet destroyed, reduce its state by 50
			else {
			ziel.setHuelleZustand(huelle - 50);
			ziel.setEnergieversorgungZustand(energieversorgung - 50);
			//if the shell after reducing is going under the 0, set the state of life system to 0 and send a message
				if (huelle <= 0) {
					ziel.setLebenserhaltungssystemeZustand(0);
					ziel.nachrichtSenden("Die Lebenserhaltungssysteme sind vernichtet worden.");
				}
			}
		}
		//if the shield is not yet destroyed, reduce it by 50
		else
			ziel.setSchutzschildeZustand(schield - 50);
	}
	
	/**
	 * method to send the message to all
	 * @param message the message that you want to send
	 */
	public void nachrichtSenden(String message) {
		this.broadcast_kommunikator.add(message);
		System.out.println("Nachricht an Alle wurde gesendet: " + message);
	}
	
	/**
	 * method to show all the message from broadcast
	 */
	public void logbuchZeigen() {
		System.out.println("Logbuch Einträge: ");
		for (String message:this.broadcast_kommunikator) {
			System.out.println(message);
		}
	}
}
